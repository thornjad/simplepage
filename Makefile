VERSION := $(shell node -p "require('./package.json').version")

all: build package

compile:
	npx coffee -o dist/ -c src/*

optimize:
	npx uglifyjs -c -m --timings --verbose -o dist/simplepage.js dist/simplepage.js

build: compile optimize

.PHONY: package
package: build
	npx web-ext build -o

test: compile
	npx snyk test --dev
	npx web-ext lint

package-version-patch:
	npm version patch

package-version-minor:
	npm version minor

package-version-major:
	npm version major

update-manifest-version:
	set -i -e 's/$(VERSION)/$(shell node -p "require('./package.json').version")/g' ./manifest.json
	git add manifest.json
	git commit --amend --no-edit

version-patch: package-version-patch update-manifest-version
version-minor: package-version-minor update-manifest-version
version-major: package-version-major update-manifest-version
