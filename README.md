# simplePage

[![pipeline status](https://gitlab.com/thornjad/simplepage/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/thornjad/simplepage/commits/master) [![ISC license](https://img.shields.io/badge/license-ISC-green.svg?style=flat-square)](https://gitlab.com/thornjad/simplepage/blob/master/LICENSE)

**simplePage** is a browser extension which makes simple pages more readable,
with less frills and less breakage than reader mode.

Once installed, click the book icon in the URL bar to toggle between simplePage
mode and normal mode.

# Install

Add to Firefox: <https://addons.mozilla.org/en-US/firefox/addon/simplepage/>

# Build

```
npm install
make
```

Copyright (c) Jade Michael Thornton under [the terms of the ISC License](./LICENSE).
