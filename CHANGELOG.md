# Changelog

# [Unreleased]

## Added
- A make all target which builds and packages the extension
- Better build documentation
- Link to add-on page

# [1.0.0] - 2019-03-26

- First release

## Changed
- Better gitignore

## Added
- Versioning script
- Makefile to replace npm scripts

# [0.2.0] - 2019-03-25

- Initial revision
