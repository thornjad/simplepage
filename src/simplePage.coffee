applyTitle = 'Simplify simple page style'
removeTitle = 'Restore original page style'
iconPath = '../icons/simplePage.svg'
simpleCss = '''
    @media screen and (min-width: 1500px) {
      body {margin: auto 27.5vw;}
    }
    @media screen and (min-width: 900px) and (max-width: 1500px) {
      body {margin: auto 17.5vw;}
    }
    @media screen and (max-width: 900px) {
      body {margin: auto 5vw;}
    }
  '''

# apply the active style to the given tab
applyStyle = (tab) ->
  if await getActive() then addStyle tab else removeStyle tab

# swap active style, apply to all tabs
toggleStyle = (tab) ->
  setActive not await getActive()
  applyStyle tab

addStyle = (tab) ->
  browser.tabs.insertCSS {code: simpleCss}
  browser.pageAction.setTitle {tabId: tab.id, title: removeTitle}

removeStyle = (tab) ->
  browser.tabs.removeCSS {code: simpleCss}
  browser.pageAction.setTitle {tabId: tab.id, title: applyTitle}

# ensure page action button matches current state
syncPageAction = (tab) ->
  useTitle = if await getActive() then removeTitle else applyTitle
  browser.pageAction.setIcon {tabId: tab.id, path: iconPath}
  browser.pageAction.setTitle {tabId: tab.id, title: useTitle}
  browser.pageAction.show tab.id

syncTab = (tab) ->
  syncPageAction tab
  applyStyle tab

syncAllTabs = ->
  tabs = await browser.tabs.query {}
  syncTab tab for tab in tabs

getActive = () ->
  tabs = await browser.tabs.query
    active: true
    currentWindow: true
  url = tabs[0].url

  cookie = await browser.cookies.get
    url: url
    name: 'simplePageActive'

  if cookie is null
    await setActive false
    false
  else
    cookie.value is 'true'

setActive = (newIsActive = 'false') ->
  newIsActive = newIsActive.toString()

  tabs = await browser.tabs.query
    active: true
    currentWindow: true
  url = tabs[0].url

  browser.cookies.set
    url: url
    name: 'simplePageActive'
    value: newIsActive

# on page update, make sure page action is loaded
browser.tabs.onUpdated.addListener (id, changeInfo, tab) -> syncTab tab

browser.pageAction.onClicked.addListener toggleStyle

# on first load, add page action to all tabs
syncAllTabs()
